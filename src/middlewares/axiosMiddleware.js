//system import file

import { get as _get, pickBy as _pickBy, isEmpty as _isEmpty } from 'lodash';
import axios from 'axios';


// user import file

import { generateV1uuid } from '../helpers/uuidGeneratorHelper';

// pure function
const addOptionalOptions = (config, options) => {
  const newOptions = { ...options };
  if (config.isFormData && _isEmpty(config.body)) {
    newOptions.body = config.formData;
  } else {
    newOptions.data = config.body;
  }

  return newOptions;
};

const httpVerbs = {
  post: 'POST',
  get: 'GET',
  put: 'PUT',
  patch: 'PATCH',
  delete: 'DELETE',
};

const axiosMiddleware = store => next => (action) => {
  if (!action || !action.fetchConfig) {
    return next(action);
  }

  const { dispatch } = store;
  const { fetchConfig: config, constants } = action;

  dispatch(config.initHandler(constants));

  const path = config.path || '/';
  const argMethod = config.method || 'GET';
  const resolve = config.resolve;
  const reject = config.reject;

  const method = httpVerbs[argMethod.toLowerCase()];

  const headers = config.headers && { ...config.headers } || {};

  const successHandler = config.success;
  const failureHandler = config.failure || function (error, errCode) {
    return {
      type: 'DUMMY_ERROR', error, errCode,
    };
  };


  const state = store.getState();
  const metaHeaders = {
    CorrelationId: generateV1uuid(),
    'Content-Type': config.contentType || 'application/json',
    'Cache-Control': 'no-store' // cache-busting,
  };
  if (config.isFormData) {
    delete metaHeaders['Content-Type'];
  }

  if (!config.doNotSendAuthHeader) {
    metaHeaders.checkauth = true;
  }

  const metaOptions = {
    url: path,
    method,
    headers: {
      ...metaHeaders,
      ...headers,
    },
  };

  let options = addOptionalOptions(config, metaOptions);

  const passOnParams = _pickBy(config.passOnParams, param => param);
  if (config.passOnParams) {
    options = {
      ...options,
      ...passOnParams,
    };
  }

  axios(
    options,
  )
    .then(json => dispatch(successHandler(json.data, constants, resolve)))
    .catch(error => dispatch(failureHandler(error, constants, reject)));
};

export default axiosMiddleware;
