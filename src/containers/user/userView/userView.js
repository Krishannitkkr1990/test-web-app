
import React from 'react';
import { Panel } from 'react-bootstrap';
import { get as _get } from 'lodash';

class userView extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      userDetails: {}
    }
  }

  componentDidMount() {
    const userDetails = JSON.parse(localStorage.getItem('userDetails'));
    this.setState({ userDetails })
  }

  render() {
    const { userDetails } = this.state;
    return (
      <div className="">
        <div class="row">
          <div class="col-sm-6">
            <ul class="breadcrumb">
              <li><a href="#/">Users List</a></li>
              <li>User Details</li>
            </ul>
          </div>
        </div>
        <div className='row' style={{ marginRight: '0px',marginLeft: "0px"}}>
          <Panel bsStyle="info">
            <Panel.Heading>
              <Panel.Title componentClass="h3">
                {_get(userDetails, 'first_name')} {_get(userDetails, 'last_name')}
              </Panel.Title>
            </Panel.Heading>
            <Panel.Body>
              <div className="row">
                <div className="col-md-4 col-sm-6 form-d">
                  <label className='user-label-text'>Company</label>
                  <span className='user-span-text'>{_get(userDetails, 'company_name')}</span>
                </div>
                <div className="col-md-4 col-sm-6 form-d">
                  <label className='user-label-text'>City</label>
                  <span className='user-span-text'>{_get(userDetails, 'city')}</span>
                </div>
                <div className="col-md-4 col-sm-6 form-d">
                  <label className='user-label-text'>State</label>
                  <span className='user-span-text'>{_get(userDetails, 'state')}</span>
                </div>
                <div className="col-md-4 col-sm-6 form-d">
                  <label className='user-label-text'>ZIP</label>
                  <span className='user-span-text'>{_get(userDetails, 'zip')}</span>
                </div>
                <div className="col-md-4 col-sm-6 form-d">
                  <label className='user-label-text'>Email</label>
                  <span className='user-span-text'>{_get(userDetails, 'email')}</span>
                </div>
                <div className="col-md-4 col-sm-6 form-d">
                  <label className='user-label-text'>Web</label>
                  <span className='user-span-text'>{_get(userDetails, 'web')}</span>
                </div>
                <div className="col-md-4 col-sm-6 form-d">
                  <label className='user-label-text'>Age</label>
                  <span className='user-span-text'>{_get(userDetails, 'age')}</span>
                </div>
              </div>
            </Panel.Body>
          </Panel>
        </div>
      </div>
    )
  }
}

export default userView;