
import React from 'react';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table'
import { Panel } from 'react-bootstrap';

const columnStyle = {
  whiteSpace: 'normal',
  wordWrap: 'break-word',
}


const options = {
  clearSearch: true,
  paginationPosition: 'bottom',
  paginationSize: 9,
  prePage: '<',
  nextPage: '>',
  withFirstAndLast: true,
  // hideSizePerPage: true,
  sizePerPageList: [{
    text: '5', value: 5,
  }],
  sizePerPage: 5, // which size per page you want to locate as default
};

class UserSearchTable extends React.Component {

  firstNameColumnFormatter = (cell, row)=>{
    return (
      <a onClick={()=>this.props.handleUserDetailsClick(row)} style={{cursor: 'pointer'}}>{cell}</a>
    )  
  }

  linkColumnFormatter = (cell,row)=> <a style={{cursor: 'pointer'}}>{cell}</a>

  render() {
    return (
      <Panel>
        <Panel.Heading>
          <Panel.Title>
              Users List
          </Panel.Title>
        </Panel.Heading>
        <Panel.Body>
          <BootstrapTable
            width=""
            data={this.props.data}
            hover
            pagination={true}
            exportCSV={true}
            options={options}
            keyField='id'
            search={true}>
            <TableHeaderColumn
              dataField='first_name'
              dataSort
              searchable={true}
              width='50'
              thStyle={columnStyle}
              tdStyle={columnStyle}
              dataFormat = {this.firstNameColumnFormatter}
            >First Name
            </TableHeaderColumn>
            <TableHeaderColumn
              dataField='last_name'
              dataSort
              searchable={true}
              width='50'
              thStyle={columnStyle}
              tdStyle={columnStyle}
            >
              Last Name
            </TableHeaderColumn>
            <TableHeaderColumn
              dataField='company_name'
              dataSort
              searchable={true}
              width='95'
              thStyle={columnStyle}
              tdStyle={columnStyle}
            >
              Company Name
            </TableHeaderColumn>
            <TableHeaderColumn
              dataField='city'
              dataSort
              searchable={true}
              width='60'
              thStyle={columnStyle}
              tdStyle={columnStyle}
            >
              City
            </TableHeaderColumn>
            <TableHeaderColumn
              dataField='state'
              dataSort
              searchable={true}
              width='40'
              thStyle={columnStyle}
              tdStyle={columnStyle}
            >
              State
            </TableHeaderColumn>
            <TableHeaderColumn
              dataField='zip'
              dataSort
              searchable={true}
              width='40'
              thStyle={columnStyle}
              tdStyle={columnStyle}
            >
              ZIP
            </TableHeaderColumn>
            <TableHeaderColumn
              dataField='email'
              dataSort
              searchable={true}
              width='95'
              thStyle={columnStyle}
              tdStyle={columnStyle}
            >
              Email
            </TableHeaderColumn>
            <TableHeaderColumn
              dataField='web'
              dataSort
              searchable={true}
              width='95'
              thStyle={columnStyle}
              tdStyle={columnStyle}
              dataFormat={this.linkColumnFormatter}
            >
              Web
            </TableHeaderColumn>
            <TableHeaderColumn
              dataField='age'
              dataSort
              searchable={true}
              width='35'
              thStyle={columnStyle}
              tdStyle={columnStyle}
            >
              Age
            </TableHeaderColumn>
          </BootstrapTable>
        </Panel.Body>
      </Panel>
    )
  }
}

export default UserSearchTable;