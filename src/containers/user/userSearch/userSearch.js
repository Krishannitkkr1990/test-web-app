
import React from 'react';
import { connect } from 'react-redux';
import { get as _get } from 'lodash';

import { getData } from '../../../actions/common';
import * as USER_CONST from '../../../constants/user';
import UserSearchTable from './component/userSearchTable';

class UserSearch extends React.Component {

  componentDidMount() {
    const userListUrl = 'https://demo9197058.mockable.io/users';
    const userListConst = {
      init: USER_CONST.USER_LIST_INIT,
      success: USER_CONST.USER_LIST_SUCCESS,
      error: USER_CONST.USER_LIST_ERROR
    }
    this.props.getUserListData(userListUrl, userListConst);
    localStorage.removeItem('userDetails');
  }

  handleUserDetailsClick = (userDetails={})=> {
    localStorage.setItem('userDetails',JSON.stringify(userDetails));
    this.props.history.push(`/user/${_get(userDetails,'id')}`);
  }

  render() {
    return (
      <div>
        <UserSearchTable
          data={_get(this.props,'userListData')}
          handleUserDetailsClick={this.handleUserDetailsClick}
        />
      </div>
    )
  }
}

const mapDispatchToProps = (dispatch) => ({
  getUserListData: (url, constants) => dispatch(getData(url, constants))
})

const mapStateToProps = (state) => ({
  isLoading: _get(state,'userReducer.isFetching'),
  userListData: _get(state,'userReducer.userListData',[])
})

export default connect(mapStateToProps, mapDispatchToProps)(UserSearch);