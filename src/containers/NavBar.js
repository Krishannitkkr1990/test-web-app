import React from 'react';
import { Link, withRouter } from 'react-router-dom';
import { get as _get } from 'lodash';
import icon_home from '../assets/images/icon_home.jpg'
import icon_contact from '../assets/images/icon_contact.png';
import icon_about from '../assets/images/icon_about.png'

class NavBar extends React.Component {

  handleSideBarMenuClick = (path) => {
    if (_get(this.props, 'location.pathname') !== path) {
      this.props.history.push(path);
    }
  }

  render() {

    const items = [{
      path: '/home',
      text: 'Home',
      iconClass: 'fa fa-home fa-2x',
    }, {
      path: '/',
      text: 'User',
      iconClass: 'fa fa-user-circle-o fa-2x'
    }, {
      path: '/contact',
      text: 'Contact',
      iconClass: 'fa fa-address-card-o fa-2x'
    }];
    
    const pathName = _get(this.props, 'location.pathname');

    return (
      <ul id="menu-bar">
        {items.map(item => (
          <li key={item.path} className={pathName === item.path ? 'menu_item active' : 'menu_item'}
            onClick={() => this.handleSideBarMenuClick(item.path)}
          >
          <Link to={item.path}>
            <span>
              <i className={item.iconClass} aria-hidden="true"></i>
            </span>
            <span className='menu-main-text'>{item.text}</span>
          </Link>  
          </li>
        ))}
      </ul>
    );
  }
}

export default withRouter(NavBar);