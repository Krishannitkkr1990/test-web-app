import React from 'react';
import { Jumbotron, FormGroup, Button, FormControl } from 'react-bootstrap';
import { connect } from 'react-redux';
import { Redirect } from 'react-router';
import { get as _get, set as _set } from 'lodash';
import { Link } from 'react-router-dom';
import FacebookLogin from 'react-facebook-login';
import GoogleLogin from 'react-google-login';
import Alert from 'react-s-alert';

function showAlert(error, msg) {
  if (error) {
    return Alert.error(msg || 'Something went wrong', {
      position: 'bottom-right',
      effect: 'slide',
      timeout: 3000,
      html: true,
    });
  } else {
    return Alert.success(msg || 'Operation Successfully', {
      position: 'bottom-right',
      effect: 'slide',
      timeout: 3000,
      html: true,
    });
  }
}

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      name: '',
      redirectToHome: false,
    };
    this.loginCredentials = {};
  }

  isValidEmailAddress() {
    const pattern = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    return pattern.test(this.state.email);
  }

  validateForm() {
    return this.state.email.length > 0 && this.isValidEmailAddress() && this.state.name.length > 0;
  }

  handleChange = (event) => {
    _set(this.loginCredentials, event.target.name, event.target.value);
    this.setState({
      [event.target.id]: event.target.value,
    });
  }

  handleSubmit = (event) => {
    showAlert(false, 'Login Successfully');
    this.setState({ redirectToHome: true });
    const data = {
      email: this.state.email,
      name: this.state.name
    }
    localStorage.setItem('userInfo', JSON.stringify(data));
  }

  getClassName() {
    if (this.validateForm()) {
      return 'info';
    }
    return 'info';
  }

  responseFacebook = (response) => {
    console.log(response);
    if (_get(response, 'email')) {
      const data = {
        email: _get(response, 'email'),
        name: _get(response, 'name')
      }
      localStorage.setItem('userInfo', JSON.stringify(data));
      showAlert(true, 'Successfully login with Facebook');
      this.props.history.push('/itemList');
    } else {
      showAlert(true, 'Login failure with Facebook')
    }
  }

  responseGoogle = (response) => {
    console.log(response);
    if (_get(response, 'profileObj')) {
      const data = {
        email: _get(response, 'profileObj.email'),
        name: _get(response, 'profileObj.name')
      }
      localStorage.setItem('userInfo', JSON.stringify(data));
      showAlert(true, 'Successfully login with Google');
      this.props.history.push('/itemList');
    } else {
      showAlert(true, 'Login failure with Google')
    }

  }

  render() {

    if (this.state.redirectToHome) {
      return (<Redirect push to={'/itemList'} />);
    }

    return (<Jumbotron>
      <div className="login">
        <GoogleLogin
          clientId="" //CLIENTID NOT CREATED YET
          buttonText="Login With Google"
          onSuccess={this.responseGoogle}
          onFailure={this.responseGoogle}
          render={renderProps => (
            <Button
              onClick={renderProps.onClick}
              block
              bsSize="large"
              className="btnGoogle"
            >LOGIN WITH GOOGLE
            </Button>
          )}
        />
        <br />
        <br />
        <FacebookLogin
          appId="" //APP ID NOT CREATED YET
          fields="name,email,picture"
          callback={this.responseFacebook}
          textButton="&nbsp;&nbsp;Login with Facebook"
          buttonStyle={{ width: '100%' }}
        />
        <div className="login-separator">
          OR
        </div>
        <form onSubmit={this.handleSubmit}>
          <FormGroup controlId="name" bsSize="large">
            <FormControl
              value={this.state.name}
              onChange={this.handleChange}
              name="name"
              type="text"
              placeholder="Name"
            />
          </FormGroup>
          <FormGroup controlId="email" bsSize="large">
            <FormControl
              autoFocus
              type="email"
              value={this.state.email}
              name="email"
              onChange={this.handleChange}
              placeholder="Email"
            />
          </FormGroup>
          <Button
            bsStyle={this.getClassName()}
            block
            bsSize="large"
            disabled={!this.validateForm()}
            type="submit"
            className="btn btn-login"
          >
            Create Account
          </Button>
        </form>
      </div>
    </Jumbotron>
    );
  }
}

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps)(Login);
