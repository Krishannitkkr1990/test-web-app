
import React, { Component } from 'react';

// This layout is used for login purpose

class EmptyLayout extends Component {
	render() {
		return (
			<div className="login-container">
				<div className="">
					{this.props.children}
				</div>
				<div className="footer">
					{`Copyright \u00A9 2019 Test App Inc.`}
				</div>
			</div>
		);
	}
}
export default EmptyLayout;
