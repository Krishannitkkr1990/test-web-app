
import React, { Component } from 'react';

import NavBar from '../NavBar';
import company_logo from '../../assets/images/company_logo.jpeg'

class MainLayout extends Component {

  render() {

    return (
      <div className='main-container'>
        <div id="header">
          <div className='left-header'>
            <a>
              <img style={{ height: '80px', width: '120px' }} src={company_logo} />
            </a>
          </div>
          <div className='middle-header'>
          </div>
          <div className='right-header'>
            <span className='right-header-icon'>
              <i className='fa fa-question-circle fa-3x'></i>
            </span>
            <span className='right-header-icon'>
              <i className='fa fa-cog fa-3x'></i>
            </span>
            <span className='right-header-icon'>
              <i className='fa fa-user-circle fa-3x'></i>
            </span>
            <span className='right-header-icon'>
              <i className='fa fa-sign-out fa-3x'></i>
            </span>
          </div>
        </div>
        <div className='content'>
          <div id="sidebar-left">
            <NavBar />
          </div>
          <div id="main">
            {this.props.children}
          </div>
          <div id="footer">
        </div>
        </div>
      </div>
    );
  }

}
export default MainLayout;

