
import React from 'react';
import { HashRouter as Router, Switch, Route } from 'react-router-dom';

import EmptyLayout from './EmptyLayout';
import MainLayout from './MainLayout';

/* Component import start */

import UserSearch from '../../containers/user/userSearch/userSearch';
import UserView from '../../containers/user/userView/userView';

/* Component import end */

function Home() {
  return (
    <div>
      Home
		</div>
  )
}

function Contact() {
  return (
    <div>
      Contact
		</div>
  )
}

function Login() {
  return (
    <div>
      Login
		</div>
  )
}

function RouteWithLayout({ layout, component, ...rest }) {
  return (
    <Route {...rest} render={(props) =>
      React.createElement(layout, props, React.createElement(component, props))
    } />
  );
}

class Layout extends React.Component {
  render() {
    return (
      <Switch>
        <RouteWithLayout exact={true} layout={MainLayout} path="/" component={UserSearch} />
        <RouteWithLayout exact={true} layout={MainLayout} path="/user/:userId" component={UserView} />
        <RouteWithLayout exact={true} layout={MainLayout} path="/contact" component={Contact} />
        <RouteWithLayout exact={true} layout={MainLayout} path="/home" component={Home} />
        <RouteWithLayout exact={true} layout={EmptyLayout} path="/login" component={Login} />
      </Switch>
    )
  }
}

export default Layout; 