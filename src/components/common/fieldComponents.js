import React from 'react';
import Datetime from 'react-datetime';
import Select from 'react-select';
import { 
  TextField,
  FormHelperText,
  InputLabel,
  Checkbox } from '@material-ui/core';


export const TextAreaInput = ({ input, rows = "10", cols = "40", label = '', disabled = false, meta: { touched, error, warning } }) => (
  <div>
    <textarea
      {...input}
      rows={rows}
      cols={cols}
      disabled={disabled}
      placeholder={label}
      error={touched && error ? true : false}
    />
    {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
  </div>
);

export const TextFieldInput = (props) => {

  let { input, label, variant = 'standard', placeholder, autoFocus, type, disabled = false, meta: { touched, error, pristine }, custom } = props;
  return (
    [<TextField
      type={type}
      label={label}
      disabled={disabled}
      placeholder={label || placeholder}
      error={touched && error ? true : false}
      autoFocus={autoFocus}
      {...input}
      {...custom}
      variant={variant}
    />,
    <div>{touched && error && <div className="text-input error"><FormHelperText >
      {error}
    </FormHelperText>
    </div>}
    </div>]
  )
}


export const CheckBoxInput = ({ input, label, disabled=false }) => (
  [
    <Checkbox
      style={{ color: '#069374' }}
      id={label}
      {...input}
      checked={input.value ? true : false}
      onChange={(event, value) => input.onChange(value)}
      disabled={disabled}
    />,
    <InputLabel htmlFor={label}>{label}</InputLabel>
  ]
)


export const DateTimePicker = (props) => {
  let { input, label, disabled = false, meta: { touched, error, warning, pristine }, custom = {} } = props;
  return (
    <React.Fragment>
      <Datetime inputProps={{ placeholder: label, disabled: disabled }} {...custom} value={input.value} {...input} />
      {touched && error &&
        <div className="text-input error">
          <FormHelperText >
            {error}
          </FormHelperText>
        </div>
      }
    </React.Fragment>
  )
}

export function ReactSelectWrapper({ input, meta: { touched, error, pristine }, disabled, onSelect, options=[], multi, className, placeholder, custPlaceHolder }) {
  const { name, value, onBlur, onChange, onFocus } = input;
  let transformedValue = transformValue(value, options, multi);
  if (transformedValue == undefined)
    transformedValue = '';
  return (

    [
      <label style={{
        display: 'inline-block',
        maxWidth: '100%',
        marginB0ttom: '0px',
        fontWeight: '000',
        color: 'rgba(0, 0, 0, 0.38)'
      }}>{placeholder}</label>,
      <Select
        disabled={disabled}
        valueKey="value"
        name={name}
        placeholder={ custPlaceHolder ||'Select'}
        value={transformedValue}
        multi={multi}
        options={options}
        onChange={multi
          ? multiChangeHandler(onChange)
          : singleChangeHandler(onChange, onSelect)
        }
        onBlur={() => onBlur(value)}
        onFocus={onFocus}
      
      />,
      <div>{touched && error && <div className="text-input error"><FormHelperText >
        {error}
      </FormHelperText>
      </div>}
      </div>
    ]

  );
}

/**
 * onChange from Redux Form Field has to be called explicity.
 */
function singleChangeHandler(func, onSelect) {
  return function handleSingleChange(value) {
    func(value ? value.value : '');
    onSelect && onSelect(value ? value : '');
  };
}

/**
 * onBlur from Redux Form Field has to be called explicity.
 */
function multiChangeHandler(func) {
  return function handleMultiHandler(values) {
    func(values.map(value => value.value));
  };
}

/**
 * For single select, Redux Form keeps the value as a string, while React Select 
 * wants the value in the form { value: "grape", label: "Grape" }
 * 
 * * For multi select, Redux Form keeps the value as array of strings, while React Select 
 * wants the array of values in the form [{ value: "grape", label: "Grape" }]
 */
function transformValue(value, options, multi) {
  if (multi)
    if (multi && typeof value === 'string') return [];

  const filteredOptions = options.filter(option => {
    return multi
      ? value.indexOf(option.value) !== -1
      : option.value === value;
  });

  return multi ? filteredOptions : filteredOptions[0];
}


