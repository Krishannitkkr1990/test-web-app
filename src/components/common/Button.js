import React from 'react';
import { Button } from 'react-bootstrap';


function GenericButton({
  btnType, btnText, btnClassName, children, onClickHandler, disabled = false,
}) {
  return (
    <Button type={btnType || 'button'} title={btnText} className={btnClassName} onClick={onClickHandler || function () { alert('no action on click') }} disabled={disabled}>
      {children}{' '}{btnText}
    </Button>
  );
}

export function EditButton({
  btnText, btnType, btnClassName, children, onClickHandler, disabled = false,
}) {
  return (
    <GenericButton btnText={btnText} disabled={disabled} btnType={btnType} btnClassName={btnClassName || 'btn btn-info'} onClickHandler={onClickHandler}>
      {children || <span className="glyphicon glyphicon-edit"></span>}
    </GenericButton>
  );
}

export function AddNewButton({
  btnText, btnType, btnClassName, children, onClickHandler, disabled = false,
}) {
  return (
    <GenericButton btnText={btnText} btnType={btnType} btnClassName={btnClassName || 'btn btn-info btn-add-new'} disabled={disabled} onClickHandler={onClickHandler}>
      {children || <span className="glyphicon glyphicon-plus-sign"></span>}
    </GenericButton>
  );
}

export function SaveButton({
  btnText, btnType, btnClassName, children, onClickHandler, disabled = false,
}) {
  return (
    <GenericButton btnText={btnText} btnType={btnType} btnClassName={btnClassName || 'btn btn-info'} disabled={disabled} onClickHandler={onClickHandler}>
      {children || <span className="glyphicon glyphicon-save"></span>}
    </GenericButton>
  );
}


export function SearchButton({
  btnText, btnType, btnClassName, children, onClickHandler, disabled = false,
}) {
  return (
    <GenericButton btnText={btnText} btnType={btnType} btnClassName={btnClassName || 'btn btn-default'} disabled={disabled} onClickHandler={onClickHandler}>
      {children || <span className="glyphicon glyphicon-search"></span>}
    </GenericButton>
  );
}

export function CancelButton({
  btnText, btnType, btnClassName, children, onClickHandler, disabled = false,
}) {
  return (
    <GenericButton btnText={btnText} btnType={btnType} btnClassName={btnClassName || 'btn btn-default'} disabled={disabled} onClickHandler={onClickHandler}>
      {children || <span className="glyphicon glyphicon-remove"></span>}
    </GenericButton>
  );
}

export function ResetButton({
  btnText, btnType, btnClassName, children, onClickHandler, disabled = false,
}) {
  return (
    <GenericButton btnText={btnText} btnType={btnType} btnClassName={btnClassName || 'btn btn-warning'} disabled={disabled} onClickHandler={onClickHandler}>
      {children || <span className="glyphicon glyphicon-refresh"></span>}
    </GenericButton>
  );
}


