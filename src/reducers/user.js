
import * as USER_CONST from '../constants/user';

const userReducer = (state = {
  type: '',
  error: '',
  isFetching: false,
  userListData: []
}, action) => {
  switch (action.type) {
    case USER_CONST.USER_LIST_INIT:
      return {
        ...state,
        type: action.type,
        isFetching: true
      }
    case USER_CONST.USER_LIST_SUCCESS:
      return {
        ...state,
        type: action.type,
        isFetching: false,
        userListData: action.data,
      }
    case USER_CONST.USER_LIST_ERROR:
      return {
        ...state,
        type: action.type,
        isFetching: false,
        error: action.error,
      }
    default:
      return state;
  }
}

export default userReducer;