
// system import file

import { combineReducers } from 'redux';

// user import file

import userReducer from './user';

const initialReducer = (state = 'initialState', action) => {
  switch (action.type) {
    default:
      return state;
  }
}

const rootReducer = combineReducers({
  initialReducer,
  userReducer
})

export default rootReducer
