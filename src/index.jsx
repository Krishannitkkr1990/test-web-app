
// system import file

import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter as Router, Switch, Route } from 'react-router-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { persistStore, persistReducer } from 'redux-persist'
import { PersistGate } from 'redux-persist/integration/react';
import storage from 'redux-persist/lib/storage';
import hardSet from 'redux-persist/lib/stateReconciler/hardSet'

import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';

// user import file
import Layout from './containers/layout/Layout';
import reducers from './reducers';

import "./assets/stylesheets/main.css";
import "react-bootstrap-table/dist/react-bootstrap-table.min.css"

// container file import start

import fetchMiddleware from './middlewares/fetchMiddleware';
import axiosMiddleware from './middlewares/axiosMiddleware';
// container file import end

const middleware = [thunk, fetchMiddleware];
middleware.push(createLogger());

const persistConfig = {
	key: 'root',
	storage,
	stateReconciler: hardSet,
	blacklist: ['someKey']
}

const persistReduced = persistReducer(persistConfig, reducers);

const store = createStore(
	persistReduced,
	applyMiddleware(...middleware)
);

const persistor = persistStore(store);



ReactDOM.render(
	<div>
		<Provider store={store}>
			<PersistGate loading={null} persistor={persistor}>
				<Router>
					<Layout />
				</Router>
			</PersistGate>
		</Provider>
	</div>,
	document.getElementById('content-wrapper'));