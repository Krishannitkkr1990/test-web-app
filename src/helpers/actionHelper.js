const dynamicActionWrapper = ({
  path,
  isFormData,
  formData,
  method,
  body,
  headers,
  initCb,
  successCb,
  failureCb,
  mode,
  redirect,
  credentials,
  cache,
  referrer,
  referrerPolicy,
  integrity,
  keepalive,
  signal,
  resolve,
  reject,
  constants
}) => ({
  constants,
  fetchConfig: {
    path,
    method,
    isFormData,
    formData,
    body,
    headers,
    initHandler: initCb,
    success: successCb,
    failure: failureCb,
    resolve,
    reject,
    passOnParams: {
      mode,
      redirect,
      credentials,
      cache,
      referrer,
      referrerPolicy,
      integrity,
      keepalive,
      signal,
    },
  },
});


export default dynamicActionWrapper;
