import dynamicActionWrapper from '../../helpers/actionHelper';

export const request = (constants) => ({
  type: constants.init,
  receivedAt: new Date()
});

export const receive = (json, constants, resolve) => {
  resolve(json);
  return {
    type: constants.success,
    data: json,
    receivedAt: new Date()
  }
};

export const receiveError = (err, constants ,reject)=> {
  reject(err);
  return {
    type: constants.error,
    error: err,
    receivedAt: new Date()
  }
}

export const getData = (url, constants) => dispatch => {
  return new Promise((resolve,reject)=> {
    dispatch(dynamicActionWrapper({
      path: url,
      method: 'get',
      initCb: request,
      successCb: receive,
      failureCb: receiveError,
      headers:{},
      constants,
      resolve,
      reject,
      redirect: 'follow'
    }))
  })
} 